package filepathconverter;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.omg.Messaging.SyncScopeHelper;

public class Main {

    public static void main(String[] args) {

        try {

            if (args == null || args.length != 2) {
                throw new IllegalArgumentException(
                        "Please use folllowing syntax to convert your path: <invert | towin | tounix >" + "'<path>'");
            } else {
                System.out.println("" + convert(args));
            }

        } catch (IllegalArgumentException ie) {
            System.out.println(ie.getMessage());
            return;
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

    }

    private static String convert(String[] args) throws IllegalArgumentException {

        if (args[0].equals("towin") && args[1] != null) {

            return args[1].replaceAll("/", "\\\\").replaceAll("'", "");

        } else if (args[0].equals("tounix") && args[1] != null) {

            return args[1].replaceAll("\\\\", "/").replaceAll("'", "");

        } else if (args[0].equals("invert") && args[1] != null) {

            if (args[1].contains("\\"))
                return args[1].replaceAll("\\\\", "/").replaceAll("'", "");
            else
                return args[1].replaceAll("/", "\\\\").replaceAll("'", "");

        } else {
            throw new IllegalArgumentException();
        }
    }

}
